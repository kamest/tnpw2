const jwt = require('jsonwebtoken')
const User = require('../models/User')
const getProperty = require('../util/util')

const getUser = async (req,token) =>{
    const decoded = jwt.verify(token, getProperty('auth.token'))
    return await User.findOne({ _id: decoded._id, 'tokens.token': token })
}

// Zaručuje přístup k endpointům pouze přihlášeným uživatelům
const auth = async (req, res, next) => {
    try {
        const token = req.header('Authorization').replace('Bearer ', '')
        let user = await getUser(req,token)

        if (!user) {
            throw new Error()
        }
        req.token = token
        req.user = user
        next()
    } catch (e) {
        res.status(401).send({ error: 'Prokažte svou totožnost.' })
    }
}

// Zaručuje přístup k endpointům pouze administrátorům
const authAdmin = async (req, res, next) => {
    try {
        const token = req.header('Authorization').replace('Bearer ', '')
        let user = await getUser(req,token)

        if (!user || (user && !user.admin)) {
            throw new Error()
        }
        req.token = token
        req.user = user
        next()
    } catch (e) {
        res.status(401).send({ error: 'Prokažte svou totožnost.' })
    }
}


module.exports = {
    auth : auth,
    authAdmin : authAdmin
}
