﻿const express = require('express')
const Comment = require('../models/Comment')
const {auth}= require('../middleware/auth')

const router = new express.Router()


// Vrátí všechny existující komentáře seřazené dle data vytvoření
router.get('/comments/:id', auth, async (req, res) => {
    const articleId = req.params.id

    try {
        const comments = await Comment
            .find({articleId: articleId})
            .sort([['created', 'descending']])
            .populate('commentAuthor')
            .lean()
            .exec();
        if (!comments) {
            return res.status(404).send()
        }
        comments.forEach(i=>{
            i.commentAuthor.forEach(j=>{
                delete j.password ;
                delete j.tokens ;
            })
        })
        res.send(comments)
    } catch (e) {
        res.status(500).send()
    }
})

// Vytvoří komentář
router.post('/comment/create', auth, async (req, res) => {
    const comment = new Comment({
        ...req.body,
        authorId: req.user._id
    })

    try {
        comment.save()
        res.status(201).send(comment)
    } catch (e) {
        res.status(400).send(e)
    }
})

// Upraví komentář (autor zůstává původní)
router.patch('/comment/update/:id', auth, async (req, res) => {
    try {
        let comment = await Comment.findById(req.params.id)
        
        if(req.user.admin || comment.authorId.toString()==req.user.id.toString()){
            const updates = Object.keys(req.body)
            updates.forEach((update) => {
                if(update != '_id'){
                    comment[update] = req.body[update]   
                }         
            })
            
            comment.save()
            res.status(200).send(comment)
        }else{
            res.status(401).send({ error: 'Unauthorized.' })    
        }
    } catch (e) {
        res.status(500).send()
    }
})

// Vymaže komentář
router.delete('/comment/remove/:id', auth, async (req, res) => {
    try {
        const comment = await Comment.findById(req.params.id)
        
        if(req.user.admin || comment.authorId.toString()==req.user.id.toString()){
            const comment = await Comment.findByIdAndRemove(req.params.id,{ useFindAndModify: false})
            res.status(200).send(comment)
        }else{
            res.status(401).send({ error: 'Unauthorized.' })    
        }
    } catch (e) {
        res.status(500).send()
    }
})


module.exports = router
