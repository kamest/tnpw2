import React from "react";
import { Route, Redirect } from "react-router-dom";

function AdminRoute({ component: Component, ...rest }) {
  let isAdmin = false;
  const item = localStorage.getItem('user');
  if(item){
    const user = JSON.parse(item);
    if(user && user.token && user.user.admin) isAdmin=true;
  }
  // pokud není uživatel administrátor nemůže na stránku admin a jiné používající AdminRoute
  return (
    <Route
      {...rest}
      render={props =>
        isAdmin ? (
          <Component {...props} />
        ) : (
          <Redirect
            to={{ pathname: "/login", state: { referer: props.location } }}
          />
        )
      }
    />
  );
}

export default AdminRoute;