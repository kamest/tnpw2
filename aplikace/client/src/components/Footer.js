import React from "react";

function Footer(props) {
  return (
    <footer className="page-footer indigo">
      <div className="footer-copyright">
        <div className="container">
        Vytvořeno Štěpánem Kameníkem jako semestrální práce v předmětu KTNPW2 na UHK. 
        </div>
      </div>
    </footer>
  );
}

export default Footer;