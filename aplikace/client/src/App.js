import React from "react";
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect
} from "react-router-dom";
import PrivateRoute from './PrivateRoute';
import AdminRoute from './AdminRoute';
import Home from "./pages/Home";
import Admin from "./pages/Admin";
import Header from "./components/Header";
import Login from "./pages/Login";
import Signup from './pages/Signup';
import Article from './pages/Article';
import Footer from './components/Footer';

function App() {

  return (
    <Router>
      <div>
        <Header/>

        <Switch>

          {/* Výpis článků */}
          <PrivateRoute exact path="/home" component={Home} />
          <PrivateRoute exact path="/" component={Home} />
          {/* Výpis článku a komentářů */}
          <PrivateRoute path="/article/:id" component={Article} />
          {/* Přihlášní */}
          <Route path="/login" component={Login} />
          {/* Registrace */}
          <Route path="/signup" component={Signup} />
          {/* Administrace uživatelů */}
          <AdminRoute path="/admin" component={Admin} />
          {/* Fallback */}
          <Redirect to='/' />
        </Switch>
        <Footer/>
      </div>
    </Router>
  );
}

export default App;