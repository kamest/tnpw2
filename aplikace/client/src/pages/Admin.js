import React from "react";
import axios from 'axios';

class Admin extends React.Component {
  state = {
    users: []
  }

  removeUser(user){
    const currentUser = JSON.parse(localStorage.getItem('user'));
    const config = {
      headers: { 
        'Authorization': `Bearer ${currentUser.token}`}
    };

    // odstranění uživatele
    axios.delete("http://localhost:3100/users/delete/"+user._id, config)
    .then(result => {
      if (result.status === 200) {
        this.componentDidMount()
      } else {
        console.log('result ' + result.status)
      }
    }).catch(e => {
      console.log('result ' + JSON.stringify(e))
    });
  }

  sendUser(describedUser){
    const user = JSON.parse(localStorage.getItem('user'));
    const config = {
      headers: { 
        'Authorization': `Bearer ${user.token}`}
    };
    if(describedUser._id == null){
      // vytvoření uživatele
      axios.post("http://localhost:3100/users/create", describedUser,config).then(result => {
        if (result.status === 201) {
          window.location.reload(false);
        }else{
          console.log('result ' + result.status)
        }
      }).catch(e => {
        console.log('result ' + JSON.stringify(e))
      });
    }else{
      // upravení uživatele
      axios.patch("http://localhost:3100/users/update/"+describedUser._id, describedUser,config).then(result => {
        if (result.status === 200 ) {
          window.location.reload();
          
          return;
        }else{
          console.log('result ' + result.status)
        }
      }).catch(e => {
        console.log('result ' + JSON.stringify(e))
      });
    }
   
  }

  componentDidMount() {
    const user = JSON.parse(localStorage.getItem('user'));
    const config = {
      headers: { 
        'Authorization': `Bearer ${user.token}`}
    };

    axios.get("http://localhost:3100/users/", config)
    .then(result => {
      if (result.status === 200) {
        this.setState({
          users: result.data
        })
      } else {
        console.log('result ' + result.status)
      }
    }).catch(e => {
      console.log('result ' + JSON.stringify(e))
    });

  }
  render(){
    const { users,describedUser } = this.state;

    return (
      <div>
        <div className="section no-pad-bot" id="index-banner">
          <div className="container">
            <div className="row">
              <div className="col s9">
                <h5>Správa uživatelů</h5>
              </div>
              <div className="col s3 right-align">
                <h5>
                  
                  <a href="/#/" 
                    className="btn-floating  btn-large waves-effect waves-light red"
                    onClick={(e)=>{
                      e.preventDefault()
                      this.setState({
                        describedUser: {}
                      })
                      this.componentDidMount()
                    }}>
                  
                    <i className="material-icons">add</i>
                  </a>
                </h5>
  
              </div>
            </div>
            <div className="row">
  
            </div>
          </div>
        </div>
        {/* Výpis uživatelů */}
        <div className="section">
          <div className="container">
            <div className="row">
              <table>
                <thead>
                  <tr>
                      <th>Name</th>
                      <th>Email</th>
                      <th>Mobile</th>
                      <th>Admin</th>
                      <th></th>
                  </tr>
                </thead>
                <tbody>
                  {users.map(user => {
                      return(
                        <tr key={user._id}> 
                          <td>{user.name}</td>
                          <td>{user.email}</td>
                          <td>{user.mobile}</td>
                          <td><i className="material-icons">{ user.admin ? 'check_box' : 'check_box_outline_blank' }</i></td>
                          <td>
                            <a href="/#/" onClick={(e)=>{
                                e.preventDefault()
                                this.setState({
                                  describedUser: user
                                })
                                this.componentDidMount()
                              }}>
                              <i className="material-icons">edit</i>
                            </a>
                            <a href="/#/"
                              onClick={(e)=>{
                                e.preventDefault()
                                this.removeUser(user)
                              }}>
                              <i className="material-icons">delete</i>
                            </a>
                          </td>
                        </tr>
                      )})}
                </tbody>
              </table>
            </div>
          </div>
        </div>
  
        {/* vytváření a editace uživatelů */}
        <div className="divider"></div>
        {describedUser ?(
            <div className="section">
            <div className="container">
            <h5>{describedUser._id? "Úprava" : "Vytvoření"} uživatele</h5>
              <form className="col s12">
                <div className="row">
                  <div className="input-field col s6">
                    <input 
                      id="name" 
                      value={this.state.describedUser.name ? this.state.describedUser.name : ""}
                      type="text" 
                      onChange={e => {
                        describedUser.name= e.target.value;
                        this.setState({
                          describedUser:describedUser
                        })
                      }}
                      className="validate"/>
                    <label htmlFor="name">Jméno</label>
                  </div>
                </div>
                <div className="row">
                  <div className="input-field col s6">
                    <input 
                      id="email" 
                      type="email" 
                      value={this.state.describedUser.email ? this.state.describedUser.email : ""}
                      onChange={e => {
                        describedUser.email= e.target.value;
                        this.setState({
                          describedUser:describedUser
                        })
                      }}
                      className="validate"/>
                    <label htmlFor="email">Email</label>
                  </div>
                  <div className="input-field col s6">
                    <input 
                      id="mobile" 
                      type="text" 
                      value={this.state.describedUser.mobile ? this.state.describedUser.mobile : ""}
                      onChange={e => {
                        describedUser.mobile= e.target.value;
                        this.setState({
                          describedUser:describedUser
                        })
                      }}
                      className="validate"/>
                    <label htmlFor="mobile">Mobile</label>
                  </div>
                </div>
                {
                  this.state.describedUser._id?"":
                  <div className="row">
                    <div className="input-field col s12">
                      <input 
                        id="password" 
                        type="password" 
                        onChange={e => {
                          describedUser.password= e.target.value;
                          this.setState({
                            describedUser:describedUser
                          })
                        }}
                        className="validate"/>
                      <label htmlFor="password">Password</label>
                    </div>
                  </div>
                }
                <div className="row">
                  <div className="input-field col s12">
                    <label>
                      <input 
                        type="checkbox" 
                        className="filled-in"
                        value={this.state.describedUser.admin ? this.state.describedUser.admin : false}
                        checked={this.state.describedUser.admin ? this.state.describedUser.admin : false}
                        onClick={e => {
                            describedUser.admin= !describedUser.admin;
                            this.setState({
                              describedUser:describedUser
                            })
                        }}/>
                              
                      <span>Admin</span>
                    </label>
                  </div>
                </div>
                
                <div className="row">
                  <div className="input-field col s12">
                    <button className="btn waves-effect waves-light" type="submit" name="action" 
                      onClick={(e)=>{
                        e.preventDefault()
                        this.sendUser(this.state.describedUser)}}>
                        Submit
                      <i className="material-icons right">send</i>
                    </button>
                  </div>
                </div>
              </form>
            </div>
          </div>):""}
      </div>
    );
  }
}

export default Admin;